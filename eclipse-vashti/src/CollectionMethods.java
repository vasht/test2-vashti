import java.util.*;

public class CollectionMethods {
	//Gets and returns the first three planets in each system in a Collection<Planets>
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> first3s = new ArrayList<Planet>();
		Collection<String> systems = new HashSet<String>();
		
		//Get all planetary systems and store them into systems Collection.
		for(Planet p : planets) {
			if(!systems.contains(p.getPlanetarySystemName())) {
				systems.add(p.getPlanetarySystemName());
			}
		}
		//Iterate each planetary system
		for(String system : systems) {
			int added = 0; //Used as a counter on how many Planet objects are added
			for(Planet p : planets) {
				//Adds the Planet Object if its planetary system
				//is equal to the current system.
				if(system.equals(p.getPlanetarySystemName())){
					first3s.add(p);
					added++;
				}
				//Stop when the first three planets are added
				if(added == 3) {
					break;
				}
			}
		}
		return first3s;
	}
}