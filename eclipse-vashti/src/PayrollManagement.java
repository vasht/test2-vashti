
public class PayrollManagement {
	public static void main(String[] args) {
		Employee[] e_arr = new Employee[5];
		e_arr[0] = new SalariedEmployee(25010);				// = 25010
		e_arr[1] = new HourlyEmployee(52,19);				// = 51376
		e_arr[2] = new UnionizedHourlyEmployee(61,11,5000);	// = 39892
		e_arr[3] = new UnionizedHourlyEmployee(61,11,3500); // = 38392
		e_arr[4] = new HourlyEmployee(73,16);				// = 60736
															//expected sum = 215406
		
		System.out.println(getTotalExpenses(e_arr));
	}
	//Uses getYearlyPay() method on all Employee objects
	//in the array and gets their sum.
	public static double getTotalExpenses(Employee[] e_arr) {
		int sum = 0;
		for(Employee e : e_arr) {
			sum += e.getYearlyPay();
		}
		return sum;
	}
}
