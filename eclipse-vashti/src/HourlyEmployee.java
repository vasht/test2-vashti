
public class HourlyEmployee implements Employee {
	private double hours;
	private double hourly;
	
	public HourlyEmployee(double hours, double hourly) {
		this.hours = hours;
		this.hourly = hourly;
	}
	//Returns yearly by multiplying hours by hourly rate x 52
	public double getYearlyPay() {
		return hours * hourly * 52;
	}
}
