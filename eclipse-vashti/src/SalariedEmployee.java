
public class SalariedEmployee implements Employee{
	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly = yearly;
	}
	//Returns yearly field
	public double getYearlyPay() {
		return yearly;
	}
}
