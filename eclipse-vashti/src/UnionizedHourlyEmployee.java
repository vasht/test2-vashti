
public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double pensionContribution;
	
	public UnionizedHourlyEmployee(int hours, int hourly, double pensionContribution) {
		super(hours,hourly);
		this.pensionContribution = pensionContribution;
	}
	//uses HourlyEmployee(super) getYearlyPay() method
	//and add the pension contribution to it.
	//Then return their sum.
	public double getYearlyPay() {
		return super.getYearlyPay() + pensionContribution;
	}
}
